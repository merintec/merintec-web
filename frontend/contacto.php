<!DOCTYPE html>
<?php 
include('../comunes/variables.php');
include('../comunes/conexion.php');
    $tituloc=$_GET['tituloc'];
?>
<html lang="es">
<head>
<meta charset="utf-8">
<title><?php echo $var_titulo; ?></title>
<link rel="icon" type="image/png" href="../favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="<?php echo $var_descripcion_contacto; ?>" />
<meta name="author" content="<?php echo $var_author; ?>" />
<!-- css -->
<link href="../css/bootstrap.min.css" rel="stylesheet" />
<link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="../css/flexslider.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
<script src="../bootstrap/js/jquery.js"> </script>
<script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
<script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
<link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>

<!-- Theme skin -->
<link href="../skins/default.css" rel="stylesheet" />
<script type="text/javascript">
    function enviar(){
        if ($("#contactform").validationEngine('validate')){
            var url="../comunes/phpmailer/envio.php"; 
            var parametros = {
                "nombre"   : $('#nombre').val(),
                "correo"   : $('#correo').val(),
                "telefono" : $('#telefono').val(),
                "titulo"  : $('#titulo').val(),
                "contenido": $('#contenido').val()
            };
            $.ajax
            ({
                type: "POST",
                url: url,
                data: parametros,
                beforeSend: function () {
                    $("#cargando").show();
                    $("#sobre").hide();
                    var espera = "Enviando Mensaje. Por favor espere...";
                    $("#resultado").html(espera);
                },

                success: function(datatemp)
                {
                    console.log(datatemp);
                    $("#cargando").hide();
                    $("#sobre").show();
                    datatemp=datatemp.split(":::");
                    var codigo=datatemp[0];
                    var mensaje=datatemp[1];
                    $("#resultado").html(mensaje);
                    setTimeout(function() {
                      $("#mensaje").fadeOut(1500);
                    },3000);
                    if (codigo==001){
                        $("#contactform")[0].reset();
                    }
                }
            });
            return false;
        }
    }
</script>
</head>
<body>
    <!-- start header -->
        <?php $base_url='../'; $activo='Contacto'; include ('../comunes/cabecera.php'); ?>
    <!-- end header -->
    <!-- start contenido -->
    <div id="wrapper">

        <div class="container centrado">
            <h1>Contactenos</h1>


            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="jumbotron" style="background-color: #B4B4B4 ">
                            <form id="contactform"  method="POST" name="contactform" onsubmit="return jQuery(this).validationEngine('validate');">
                           
                                 <div class="row">

                                    <div class="col-md-1 col-xs-2">
                                        <i class="fa fa-user fa-2x" aria-hidden="true">  </i>
                                    </div>


                                    <div class="col-md-11 col-xs-10">
                                        
                                        <input type="text" name="nombre" id="nombre" placeholder="* Tu nombre y apellido" class="validate[required, minSize[3],maxSize[100]] text-input">
                                       
                                   </div>
                                </div>

                                 <div class="row">
                                    <div class="col-md-1 col-xs-2">
                                        <i class="fa fa-envelope fa-2x" aria-hidden="true">  </i>
                                    </div>
                                    <div class="col-md-11 col-xs-10 field">
                                        <input type="text" name="correo" id="correo" placeholder="* Tu correo electrónico"  class="validate[required, custom[email] , minSize[3],maxSize[200]] text-input">

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1 col-xs-2">
                                        <span style="font-size:2.2em;" class="glyphicon glyphicon-phone" aria-hidden="true">  </span>
                                    </div>
                                    <div class="col-md-11 col-xs-10">
                                        
                                        <input type="text" name="telefono" id="telefono" placeholder="* Número de Teléfono" class="validate[required,custom[phone], minSize[3],maxSize[100]] text-input">
                                       
                                   </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-1 col-xs-2">
                                        <i class="fa fa-text-width fa-2x"  aria-hidden="true">  </i>
                                    </div>
                                    <div class="col-md-11 col-xs-10 field">
                                        <input type="text" name="titulo" id="titulo" placeholder="* Titulo del Mensaje" class="validate[required, minSize[3],maxSize[100]] text-input" value="<?php echo $tituloc; ?>">

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1 col-xs-2">
                                        <i class="fa fa-align-justify fa-x2" aria-hidden="true">  </i>
                                    </div>
                                    <div class="col-md-11 col-xs-10 margintop10 field">
                                        <textarea rows="8" name="contenido" id="contenido"  placeholder="* Tu mensaje aquí..." class="validate[required]"></textarea>
                                            <div id="resultado"></div>
                                            <span class="pull-left margintop20" style="font-size:0.8em;">* Por favor llena los campos requeridos (*)</span>

                                            
                                          <button name="boton" id="boton" class="btn btn-theme margintop10 pull-right" onclick="enviar();return false;">Enviar &nbsp;<span id="cargando"><img src="<?php echo $base_url; ?>img/cargando_gray.gif" title="cargando..." alt="procesando..."></span><script type="text/javascript">$("#cargando").hide()</script><span id="sobre" class="fa fa-envelope"></span></button>                                       
                                    </div>
                                </div>
                                
                            </form>
                      
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <div class="row">

                            <div style="margin-top: 2em">
                                <iframe class="hidden-xs" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7891.01545149587!2d-71.24242300000002!3d8.547077!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1bc5f6a6e0f60c2f!2sA.C.+Merintec%2C+R.L!5e0!3m2!1ses-419!2sus!4v1448660592102" width="400" height="250" style="border:0" allowfullscreen></iframe>               
                                <div class="visible-xs footer-texto x075"><a href="https://www.google.com/maps/place/A.C.+Merintec,+R.L/@8.547077,-71.242423,16z/data=!4m2!3m1!1s0x0:0x1bc5f6a6e0f60c2f?hl=es-419"><img class="ancho_20" src="<?php echo $base_url; ?>img/google-map-icon.png" title="Google Maps" alt="Mapa de Google"><br><br>Ver en Google Maps<br><br></a></div>
                            </div>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-3 col-xs-3">
                                <img  class="img-circle hidden-xs ancho_60"  src="../img/fotos/juan_marquez.jpg" alt="gerente, programador, desarrollador, developer, CEO">
                                 <img   class="img-circle visible-xs ancho_100"  src="../img/fotos/juan_marquez.jpg" alt="gerente, programador, desarrollador, developer, CEO">

                            </div>
                            <div class="col-md-9 col-xs-9 text-center">
                                <p class="justificado">
                                   <A HREF="mailto:juanjmt@gmail.com" title="juanjmt@gmail.com">Ing. Juan José Márquez</A>
                                    CEO - Gerente General de Merintec. <br>
                                    Ing. en Informática, Especialista en Desarollo de Aplicaciones, SEO.<br>
                                    Desarrollador Senior PHP -MYSQL - HTML5 - CSS3 - BOOTSTRAP.<br>
                                </p>

                                    <div class="col-md-4 col-xs-4"> 
                                       <div class="circulo"> <a target="_blank" href="https://twitter.com/juanjmt"> <i class="fa fa-twitter fa-2x" aria-hidden="true"></i> </a> </div>
                                    </div>
                                    <div class="col-md-4 col-xs-4"> 
                                        <div class="circulo"> <a target="_blank" href="https://www.facebook.com/juanjmt"> <i class="fa fa-facebook fa-2x" aria-hidden="true"></i> </a></div>
                                     </div>
                                    <div class="col-md-4 col-xs-4 ">
                                       <div class="circulo"> <a target="_blank" href="https://www.instagram.com/juanjjmt/"> <i class="fa fa-instagram fa-2x" aria-hidden="true"></i> </a> </div>
                                    </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-3">
                                <img  class="img-circle hidden-xs ancho_60"  src="../img/fotos/luis_marquez.jpg" alt="gerente, programador, desarrollador, developer, CEO">
                                <img   class="img-circle visible-xs ancho_100"  src="../img/fotos/luis_marquez.jpg" alt="gerente, programador, desarrollador, developer, CEO">

                            </div>
                            <div class="col-md-9 col-xs-9 text-center">
                                <p class="justificado">
                                    <A HREF="mailto:felixpe09@gmail.com" title="felixpe09@gmail.com">Ing. Luis Felipe Márquez</A>    CEO - Gerente de Operaciones Merintec. <br>
                                    Ing. en Informática, Especialista en Desarollo de Aplicaciones, SEO.<br>
                                    Desarrallodor Senior PHP -MYSQL - CSS3 - JAVASCRIPT.<br>
                                </p>
                                 <div class="col-md-4 col-xs-4"> 
                                      <div class="circulo">  <a target="_blank" href="https://twitter.com/felixpe09"> <i class="fa fa-twitter fa-2x" aria-hidden="true"></i> </a> </div>
                                    </div>
                                    <div class="col-md-4 col-xs-4"> 
                                         <div class="circulo"> <a target="_blank" href="https://www.facebook.com/profile.php?id=100005061768398"> <i class="fa fa-facebook fa-2x" aria-hidden="true"></i> </a> </div> 
                                     </div>
                                    <div class="col-md-4 col-xs-4">
                                      <div class="circulo">  <a target="_blank" href="https://www.instagram.com/felixpe_09/"> <i class="fa fa-instagram fa-2x" aria-hidden="true"></i> </a> </div>
                                    </div>
                            </div>
                        </div>


                    </div>

                </div>


            </div>
              
                    
               
        </div>
    </div>
    <!-- end contenido -->
    <!-- start Footer -->
        <?php include ('../comunes/footer.php'); ?>
    <!-- end Footer -->
<a href="#" class="scrollup"><i class="fa fa-angle-up active" title="ir al arriba"></i></a>
<!-- javascript ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="../js/jquery.easing.1.3.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../js/jquery.fancybox.pack.js"></script>
<script src="../js/jquery.fancybox-media.js"></script>
<script src="../js/google-code-prettify/prettify.js"></script>
<script src="../js/portfolio/jquery.quicksand.js"></script>
<script src="../js/portfolio/setting.js"></script>
<script src="../js/jquery.flexslider.js"></script>
<script src="../js/animate.js"></script>
<script src="../js/custom.js"></script>
</body>
</html>