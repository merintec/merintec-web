<!DOCTYPE html>
<?php include('../comunes/variables.php'); ?>
<html lang="es">
<head>
<meta charset="utf-8">
<title><?php echo $var_titulo; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="<?php echo $var_descripcion_soporte; ?>" />
<meta name="author" content="<?php echo $var_author; ?>" />
<!-- css -->
<link href="../css/bootstrap.min.css" rel="stylesheet" />
<link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="../css/flexslider.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
<!-- Theme skin -->
<link href="../skins/default.css" rel="stylesheet" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<div id="wrapper">
	<!-- start header -->
		<?php $base_url='../'; include ('../comunes/cabecera.php'); ?>
	<!-- end header -->
		<div class="container">
			
				<br>
				<article>
					<div class="row">
						<div class="col-md-12 col-xs-12 text-center"> 
							<h1 class="hidden-xs" style="margin-bottom: 0px;"><span class="subtitulo1">Soporte Técnico</span> Especializado</h1>
							<h3 class="visible-xs" style="margin-bottom: 0px;"><span class="subtitulo1">Soporte Técnico</span>Especializado</h3>	
						</div>
					</div>	
					<div class="row">
						<div class="col-md-1 col-xs-1" >
						</div>
						<div class="col-md-10 col-xs-10 text-justify">	
								Si necesitas que tus equipos de computación funcionen en óptimas condiciones, para que el trabajo del día a día no se detenga, que estos se conecten a tus diferentes dispositivos y que la conexión a internet no sea una traba para llevar a cabo todas las tareas planteadas, <b> Merintec es la solución  </b>  contamos con el mas calificado personal para abordar todo estos detalles, atendiéndolos de forma eficiente y con profesionalismo, según el tipo de trabajo y donde se encuentre localizado brindamos los siguientes servicios:  <br>
						</div>
						<div class="col-md-1 col-xs-1" >
						</div>
					</div>	
					<div class="row visible-xs">
						<div class="col-xs-12 text-center">	
							<img src="../img/site/soporte-tecnico.jpg" class="ancho_70" title="servicio técnico, mantenimiento, instalación, reparación, computadores, redes" alt="Servicio profesinal de mantenimiento y reparación de computadoras y laptops, diseño, instalación y configuración de redes">
						</div>

					</div>
					<div class="row">
						<div class="col-md-1 col-xs-1" >
						</div>
						<div class="col-md-5 col-xs-12 text-justify">	
							1) Respaldo de Información. <br>
							2) Instalación de Sistema Operativo el de su preferencia. <br>
							3) Instalación de Aplicaciones. <br>
							4) Eliminación de Virus. <br>
							5) Instalación de Periféricos. <br>
							6) Reparación de Hardware del equipo. <br>
							7) Reemplazo de Piezas. <br>
							8) Mantenimiento de Hardware y Software. <br>
							9) Diseño de Redes <br>
							10) Instalación y Configuración de Redes LAN - Wifi. <br>
						</div>
						<div class="col-md-5 hidden-xs text-center">	
							<img src="../img/site/soporte-tecnico.jpg" class="ancho_70" title="servicio técnico, mantenimiento, instalación, reparación, computadores, redes" alt="Servicio profesinal de mantenimiento y reparación de computadoras y laptops, diseño, instalación y configuración de redes">
						</div>
						<div class="col-md-1 col-xs-1" >
						</div>
					</div>
				</article>

				<div class="row">				
					<div class="col-md-1  hidden-xs">
					</div>					
					<div class="box">
						<div class="box-gray aligncenter col-md-3 col-xs-12">
							<h4>Personas Naturales</h4>
							<div class="icon">
								<i class="fa fa-wrench fa-3x"></i>
							</div>					
							<p> Si su equipo presenta fallas sea PC o Laptop puede dirigirte a nuestras instalaciones, se realiza diagnostico de la falla totalmente gratis, somos especialista Windows y Linux cualquiera de sus versiones y distribuciones.  </p> 
						</div>
					</div>
					<div class="col-md-1 hidden-xs" >
					</div>						
					<div class="box">
						<div class=" box-gray aligncenter col-md-3 col-xs-12">	
							<h4>Visitamos su Empresa</h4>
							<div class="icon">
								<i class="fa fa-building-o fa-3x"></i>
							</div>	
							<p> Si en su empresa necesitan asistencia técnica, nos dirigirnos hacia su sede y realizamos el trabajo en el sitio, contamos con un personal confiable que brindara un trabajo profesional sin entorpecer sus laborares. </p> 
						</div>
					</div>
					<div class="col-md-1 hidden-xs" >
					</div>

					<div class="box">
						<div class=" box-gray aligncenter col-md-3 col-xs-12">	
							<h4>Asistencia Remota</h4>
							<div class="icon">
								<i class="fa fa-laptop fa-3x"></i>
							</div>	
							<p> Si urge solucionar algún detalle técnico, por medio de una llamada telefónica o vía correo electrónico haremos el primer contacto, y desde donde estemos vía remota haremos el trabajo a su equipo.                             </p> 
						</div>
					</div>
				</div>		
		</div>
	<!-- end contenido -->
	<!-- start Footer -->
		<?php $base_url='../'; include ('../comunes/footer.php'); ?>
	<!-- end Footer -->
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active" title="ir al arriba"></i></a>
<!-- javascript ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../bootstrap/js/jquery.js"></script>
<script src="../js/jquery.easing.1.3.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.fancybox.pack.js"></script>
<script src="../js/jquery.fancybox-media.js"></script>
<script src="../js/google-code-prettify/prettify.js"></script>
<script src="../js/portfolio/jquery.quicksand.js"></script>
<script src="../js/portfolio/setting.js"></script>
<script src="../js/jquery.flexslider.js"></script>
<script src="../js/animate.js"></script>
<script src="../js/custom.js"></script>
</body>
</html>