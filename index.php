<!DOCTYPE html>
<?php include('comunes/variables.php'); ?>
<html lang="es">
<head>
<meta charset="utf-8">
<link rel="alternate" href="//www.merintec.com.ve/" hreflang="x-default" />
<link rel="alternate" href="//www.merintec.com.ve/" hreflang="es-ve" />
<title><?php echo $var_titulo; ?></title>
<link rel="icon" type="image/png" href="favicon.ico">
<link href="http://merintec.com.ve/img/logo.png" rel="image_src"/>
<link rel="canonical" href="http://www.merintec.com.ve/" />
<link rel="shortlink" href="http://www.merintec.com.ve/" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="<?php echo $var_descripcion; ?>" />
<meta name="Robots" content="Index,Follow" />
<meta name="GOOGLEBOT" content="NOODP"/>
<meta name="Keywords" content="<?php echo $var_keywords; ?>"/>
<meta name="author" content="<?php echo $var_author; ?>" />
<meta name="revisit-after" content="1 days"/>
<?php echo $analyticsgoogle; ?>
<meta property="og:description" content="<?php echo $var_descripcion; ?>"/>
<meta property="og:title" content="Merintec. Tu aliado tecnológico. Creación de Páginas WEB, Reparación de Computadoras, Elaboración de Sistemas, Mérida Venezuela" />
<meta property="og:type" content="website" />
<meta property="og:image" content="http://merintec.com.ve/img/web-merintec.jpg" />
<meta property="og:image" content="http://merintec.com.ve/img/logo2.png" />
<meta property="og:url" content="http://www.merintec.com.ve/" />
<meta property="og:site_name" content="Merintec" />
<meta property="og:locale" content="es_LA" />
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "Merintec",
  "description": "<?php echo $var_descripcion; ?>",
  "image" : "http://merintec.com.ve/img/logo.png",
  "telephone" : "+582742211508",
  "email" : "contacto@merintec.com.ve",
  "address" : {
    "@type" : "PostalAddress",
    "addressLocality" : "Mérida",
    "addressRegion" : "Mérida",
    "addressCountry" : "Venezuela"
  },
  "contactPoint" : [
    { "@type" : "ContactPoint",
      "telephone" : "+58-274-2211508",
      "contactType" : "customer service",
      "areaServed" : ["VE"],
      "availableLanguage" : ["Spanish"]
    } ],
  "alumni": [
    {
      "@type": "Person",
      "name": "Juan Márquez"
    },
    {
      "@type": "Person",
      "name": "Luis Márquez"
    }
  ],
  "url" : "http://www.merintec.com.ve/"
}
</script>
<script type="text/javascript">function ir_a(a){if(a){$("html,body").animate({scrollTop:$("#"+a).offset().top},1000)}};</script>
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="css/flexslider.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="skins/default.css" rel="stylesheet" />
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<div id="wrapper">
<?php $base_url=''; include ('comunes/cabecera.php'); ?>
<section id="featured">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div id=main-slider class="flexslider">
<ul class="slides">
<?php 
/* Imagen para celebracion de navidad 2016
<li>
<img src="img/slides/navidad2016.jpg" title="WEB Responsive Desing" alt="WEB Responsive Desing"/>
<div class="flex-caption1 hidden-xs">
<h1 class="pull-right"><span class="fa fa-gift"></span> AC Merintec RL <span class="fa fa-gift"></span></h1>
<p class="pull-right">Les desea una muy Felíz Navidad y éxitos en el venidero año 2017</p>
</div>
</li>
*/ ?>
<li>
<img src="img/slides/1.jpg" title="WEB Responsive Desing" alt="WEB Responsive Desing"/>
<div class="flex-caption1 hidden-xs">
<h1>WEB Responsive Desing <span class="fa fa-html5"></span></h1>
<p>Diseñamos tu Sitio WEB compatible con diferentes plataformas, bajo los entándares de desarrollo más actualizados.</p>
<a href="frontend/diseno-desarrollo-web.php" class="btn boton_vermas">Ver más...</a>
</div>
<div class="flex-caption1-xs visible-xs">
<a href="frontend/diseno-desarrollo-web.php" class="btn boton_vermas">Diseño WEB&nbsp;&nbsp;<span class="fa fa-html5"></span></a>
</div>
</li>
<li>
<img src="img/slides/2.jpg" alt="Soporte Técnico" title="Soporte Técnico"/>
<div class="flex-caption2 hidden-xs">
<h1>Soporte Técnico</h1>
<p>Disponemos de técnicos altamente capacitados en soporte, para que tu o tu empresa no se detengan y alcancen el mayor rendimiento de sus equipos de computación.</p>
<a href="frontend/soporte-tecnico-redes.php" class="btn boton_vermas">Ver más...</a>
</div>
<div class="flex-caption2-xs visible-xs">
<a href="frontend/soporte-tecnico-redes.php" class="btn boton_vermas">Soporte Técnico&nbsp;&nbsp;<span class="fa fa-wrench"></span></a>
</div>
</li>
<li>
<img src="img/slides/3.jpg" alt="Desarrollo de Aplicaciones" title="Desarrollo de Aplicaciones" />
<div class="flex-caption3 hidden-xs">
<h1>Desarrollo de Sistemas</h1>
<p>Automatiza los procesos de tu empresa con la ayuda de aplicaciones desarrolladas a la medida, obtén sorprendentes resultados y posiciónate como el líder del mercado.</p>
<a href="frontend/desarrollo-programacion-sistemas.php" class="btn boton_vermas">Ver más...</a>
</div>
<div class="flex-caption3-xs visible-xs">
<a href="frontend/desarrollo-programacion-sistemas.php" class="btn boton_vermas">Desarrollo&nbsp;&nbsp;<span class="fa fa-code"></span></a>
</div>
</li>
<li>
<img src="img/slides/4.jpg" alt="Redes Informáticas" title="Redes Informáticas" />
<div class="flex-caption4 hidden-xs">
<h1>Redes Informáticas</h1>
<p>Optimiza tu red de computadores y sácale el máximo sin quedar enredado entre cables en tu hogar o empresa.</p>
<a href="frontend/soporte-tecnico-redes.php" class="btn boton_vermas">Ver más...</a>
</div>
<div class="flex-caption4-xs visible-xs">
<a href="frontend/soporte-tecnico-redes.php" class="btn boton_vermas">Redes&nbsp;&nbsp;<span class="fa fa-sitemap"></span></a>
</div>
</li>
<li>
<img src="img/slides/5.jpg" alt="Consultores SEO" title="Consultores SEO" />
<div class="flex-caption5 hidden-xs">
<h1>Consultores SEO</h1>
<p>¿Tienes un sito WEB con pocas Visitas?<br>Optimízalo para los mejores buscadores y recibe más visitas, que se convertirán en tus futuros clientes.</p>
<a href="frontend/diseno-desarrollo-web.php" class="btn boton_vermas">Ver más...</a>
</div>
<div class="flex-caption5-xs visible-xs">
<a href="frontend/diseno-desarrollo-web.php" class="btn boton_vermas">Asesores SEO&nbsp;&nbsp;<span class="fa fa-search"></span></a>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
</section>
<section class="callaction">
<div class="container">
<div class="row margin0">
<div class="col-xs-12">
<div class="big-cta">
<div class="cta-text">
<div id="servicios"></div><h1 class="hidden-xs"><span class="subtitulo1">Merintec</span> tu aliado tecnológico</h1><h2 class="visible-xs"><span class="subtitulo1">Merintec</span> tu aliado tecnológico</h2>
</div>
</div>
</div>
</div>
</div>
</section>
<section id="content">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="row">
<div class="col-xs-12 col-md-3">
<div class="box">
<div class="box-gray aligncenter">
<h3>Soporte Técnico</h3>
<div class="icon">
<i class="fa fa-wrench fa-3x"></i>
</div>
<p>
Especialistas en el área de mantenimiento y reparación de equipos de computación.
</p>
</div>
<a href="frontend/soporte-tecnico-redes.php">
<div class="box-bottom">
Ver más...
</div>
</a>
</div>
</div>
<div class="col-xs-12 col-md-3">
<div class="box">
<div class="box-gray aligncenter">
<h3>Diseño WEB</h3>
<div class="icon">
<i class="fa fa-html5 fa-3x"></i>
</div>
<p>
Innovadores e impresionantes diseños para sitios web que harán realzar tu negocio.
</p>
</div>
<a href="frontend/diseno-desarrollo-web.php">
<div class="box-bottom">
Ver más...
</div>
</a>
</div>
</div>
<div class="col-xs-12 col-md-3">
<div class="box">
<div class="box-gray aligncenter">
<h3>Redes&nbsp;</h3>
<div class="icon">
<i class="fa fa-sitemap fa-3x"></i>
</div>
<p>
Analisis, diseño, optimización, instalación y/o mantenimiento de tu red de área local.
</p>
</div>
<a href="frontend/soporte-tecnico-redes.php">
<div class="box-bottom">
Ver más...
</div>
</a>
</div>
</div>
<div class="col-xs-12 col-md-3">
<div class="box">
<div class="box-gray aligncenter">
<h3>Sistemas&nbsp;</h3>
<div class="icon">
<i class="fa fa-code fa-3x"></i>
</div>
<p>
Sistemas informáticos adaptados a tu medida, desarrollados con los mejores estándares.
</p>
</div>
<a href="frontend/desarrollo-programacion-sistemas.php">
<div class="box-bottom">
Ver más...
</div>
</a>
</div>
</div>
</div>
</div>
</div>
<section class="callaction">
<div class="container">
<div class="row margin0">
<div class="col-xs-12">
<div class="big-cta">
<div class="cta-text">
<div id="planes"></div><h2>Nuestros Planes Web</h2>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="row">
<div class="col-xs-12 col-md-4">
<?php include('frontend/plan_web1.php'); ?>
</div>
<div class="col-xs-12 col-md-4">
<?php include('frontend/plan_web2.php'); ?>
</div>
<div class="col-xs-12 col-md-4">
<?php include('frontend/plan_web3.php'); ?>
</div>
</div>
<section id="projects">
<div class="row">
<div class="col-xs-12">
<div id="trabajos"></div> <h3 class="heading">Trabajos Recientes</h3>
<div class="row">
<ul id="thumbs" class="portfolio">
<li class="col-xs-6 col-md-3 design" data-id="id-0" data-type="web">
<div class="item-thumbs">
<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="<a target='_blank' href='http://samatvigia.com.ve'> samatvigia.com.ve </a>" href="img/works/1.jpg">
<span class="overlay-img"></span>
<span class="overlay-img-thumb font-icon-plus"></span>
</a>
<img src="img/works/1.jpg" alt="Sitio web del órgano recaudador de impuestos del Municipio Alberto Adriani, estado Mérida. " title="Samat El Vigia">
</div>
</li>
<li class="item-thumbs col-xs-6 col-md-3 design" data-id="id-1" data-type="icon">
<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="<a target='_blank' href='http://www.upalopa.com'> www.upalopa.com </a>" href="img/works/2.png">
<span class="overlay-img"></span>
<span class="overlay-img-thumb font-icon-plus"></span>
</a>
<img src="img/works/2.png" alt="Tienda en línea de ropa de moda para niños y niñas." title="Upalopa">
</li>
<li class="item-thumbs col-xs-6 col-md-3 photography" data-id="id-2" data-type="illustrator">
<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="<a target='_blank' href='http://barvo.com.ve'> www.barvo.com.ve </a>" href="img/works/3.jpg">
<span class="overlay-img"></span>
<span class="overlay-img-thumb font-icon-plus"></span>
</a>
<img src="img/works/3.jpg" alt="Barvo. Fabrica de raviolis, pasta, salsas, pizzas y muchos mas!" title="Barvo">
</li>
<li class="item-thumbs col-xs-6 col-md-3 photography" data-id="id-2" data-type="illustrator">
<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="<a target='_blank' href='http://siems.com.ve'> www.siems.com.ve </a>" href="img/works/4.png">
<span class="overlay-img"></span>
<span class="overlay-img-thumb font-icon-plus"></span>
</a>
<img src="img/works/4.png" alt="Siems Instituto Gerencial, empresa reconocida en el área de capacitación, ofrece programas de educación continua como diplomados, cursos, seminarios y conferencias de alto nivel." title="Siems Instituto Gerencial"></li>
</ul>
</div>
</div>
</div>
</section>
</div>
</section>
<?php $base_url=''; include ('comunes/footer.php'); ?>
</div>
<a href="#"" class="scrollup"><i class="fa fa-angle-up active" title="ir al arriba"></i></a>
<script src="bootstrap/js/jquery.js"></script>
<?php /* nieve <script src="js/jquery.snow.min.1.0.js"></script> */ ?>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/portfolio/jquery.quicksand.js"></script>
<script src="js/portfolio/setting.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
<?php /* Nieve
<script>
$(document).ready( function(){
    $.fn.snow({ minSize: 10, maxSize: 50, newOn: 700, flakeColor: '#FFFFFF' });
    $.fn.snow({ minSize: 10, maxSize: 50, newOn: 700, flakeColor: '#0099FF' });

});
</script> */ ?>
</body>
</html>