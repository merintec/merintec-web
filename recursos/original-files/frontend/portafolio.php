<!DOCTYPE html>
<?php include('../comunes/variables.php'); ?>
<html lang="es">
<head>
<meta charset="utf-8">
<title><?php echo $var_titulo; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="<?php echo $var_descripcion; ?>" />
<meta name="author" content="<?php echo $var_author; ?>" />
<!-- css -->
<link href="../css/bootstrap.min.css" rel="stylesheet" />
<link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="../css/jcarousel.css" rel="stylesheet" />
<link href="../css/flexslider.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
<!-- Theme skin -->
<link href="../skins/default.css" rel="stylesheet" />
</head>
<body>
    <!-- start header -->
        <?php $base_url='../'; $activo='Contacto'; include ('../comunes/cabecera.php'); ?>
    <!-- end header -->
    <!-- start contenido -->
    <div id="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
	                            <li><a href="../index.php"><i class="fa fa-home"></i> Inicio</a><i class="icon-angle-right"></i></li>
						<li class="active">Trabajos Recientes</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="clearfix">
					</div>
					<div class="row">
						<ul id="thumbs" class="portfolio">
							<!-- Item Project and Filter Name -->
							<li class="item-thumbs col-lg-3 design" data-id="id-0" data-type="web">
							<!-- Fancybox - Gallery Enabled - Title - Full Image -->
							<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Portfolio name" href="../img/works/1.jpg">
							<span class="overlay-img"></span>
							<span class="overlay-img-thumb font-icon-plus"></span>
							</a>
							<!-- Thumb Image and Description -->
							<img src="../img/works/1.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
							</li>
							<!-- End Item Project -->
							<!-- Item Project and Filter Name -->
							<li class="item-thumbs col-lg-3 design" data-id="id-1" data-type="icon">
							<!-- Fancybox - Gallery Enabled - Title - Full Image -->
							<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Portfolio name" href="../img/works/2.jpg">
							<span class="overlay-img"></span>
							<span class="overlay-img-thumb font-icon-plus"></span>
							</a>
							<!-- Thumb Image and Description -->
							<img src="../img/works/2.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
							</li>
							<!-- End Item Project -->
							<!-- Item Project and Filter Name -->
							<li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="graphic">
							<!-- Fancybox - Gallery Enabled - Title - Full Image -->
							<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Portfolio name" href="../img/works/3.jpg">
							<span class="overlay-img"></span>
							<span class="overlay-img-thumb font-icon-plus"></span>
							</a>
							<!-- Thumb Image and Description -->
							<img src="../img/works/3.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
							</li>
							<!-- End Item Project -->
							<!-- Item Project and Filter Name -->
							<li class="item-thumbs col-lg-3 design" data-id="id-0" data-type="web">
							<!-- Fancybox - Gallery Enabled - Title - Full Image -->
							<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Portfolio name" href="../img/works/4.jpg">
							<span class="overlay-img"></span>
							<span class="overlay-img-thumb font-icon-plus"></span>
							</a>
							<!-- Thumb Image and Description -->
							<img src="../img/works/4.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
							</li>
							<!-- End Item Project -->
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- start Footer -->
        <?php include ('../comunes/footer.php'); ?>
    <!-- end Footer -->
<a href="#" class="scrollup"><i class="fa fa-angle-up active" title="ir al arriba"></i></a>
<!-- javascript ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery.js"></script>
<script src="../js/jquery.easing.1.3.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.fancybox.pack.js"></script>
<script src="../js/jquery.fancybox-media.js"></script>
<script src="../js/google-code-prettify/prettify.js"></script>
<script src="../js/portfolio/jquery.quicksand.js"></script>
<script src="../js/portfolio/setting.js"></script>
<script src="../js/jquery.flexslider.js"></script>
<script src="../js/animate.js"></script>
<script src="../js/custom.js"></script>
</body>
</html>