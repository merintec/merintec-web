<!DOCTYPE html>
<?php include('../comunes/variables.php'); ?>
<html lang="es">
<head>
<meta charset="utf-8">
<title><?php echo $var_titulo; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="<?php echo $var_descripcion; ?>" />
<meta name="author" content="<?php echo $var_author; ?>" />
<!-- css -->
<link href="../css/bootstrap.min.css" rel="stylesheet" />
<link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="../css/jcarousel.css" rel="stylesheet" />
<link href="../css/flexslider.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
<!-- Theme skin -->
<link href="../skins/default.css" rel="stylesheet" />
</head>
<body>
    <!-- start header -->
        <?php $base_url='../'; $activo='Contacto'; include ('../comunes/cabecera.php'); ?>
    <!-- end header -->
    <!-- start contenido -->
    <div id="wrapper">
        <div class="container">
            Soporte
        </div>
    </div>
    <!-- end contenido -->
    <!-- start Footer -->
        <?php include ('../comunes/footer.php'); ?>
    <!-- end Footer -->
<a href="#" class="scrollup"><i class="fa fa-angle-up active" title="ir al arriba"></i></a>
<!-- javascript ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery.js"></script>
<script src="../js/jquery.easing.1.3.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.fancybox.pack.js"></script>
<script src="../js/jquery.fancybox-media.js"></script>
<script src="../js/google-code-prettify/prettify.js"></script>
<script src="../js/portfolio/jquery.quicksand.js"></script>
<script src="../js/portfolio/setting.js"></script>
<script src="../js/jquery.flexslider.js"></script>
<script src="../js/animate.js"></script>
<script src="../js/custom.js"></script>
</body>
</html>