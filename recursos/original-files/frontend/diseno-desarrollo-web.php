<!DOCTYPE html>
<?php include('../comunes/variables.php'); ?>
<html lang="es">
<head>
<meta charset="utf-8">
<title><?php echo $var_titulo; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="<?php echo $var_descripcion; ?>" />
<meta name="author" content="<?php echo $var_author; ?>" />
<!-- css -->
<link href="../css/bootstrap.min.css" rel="stylesheet" />
<link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="../css/jcarousel.css" rel="stylesheet" />
<link href="../css/flexslider.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
<!-- Theme skin -->
<link href="../skins/default.css" rel="stylesheet" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<div id="wrapper">
	<!-- start header -->
		<?php $base_url='../'; include ('../comunes/cabecera.php'); ?>
	<!-- end header -->
	<section id="featured">
	
		<div class="container">

				<div class="row">
					<div class="col-md-12 col-xs-12 text-center">	<h1 style="margin-bottom: 0px;"><span class="subtitulo1">Diseño y Desarrollo</span> Web</h1> </div>
				</div>	
				<div class="row">

						<article> 
							<div class="col-xs-12 col-md-4">
								<div class="">
									<div class="box-gray aligncenter">
										
										<img src="../img/site/responsive.png" class="ancho_80" title="Diseño Web" alt="Diseño Web adaptado a todos los dispositivos Tecnologicos">
										<h4>Diseño Web</h4>
											Creamos la interfaz de usuario de manera profesional, siguiendo la Identidad de su producto u Organización. 
									</div>
									
								</div>
							</div>
						</article>
						<article> 
							<div class="col-xs-12 col-md-4">
								<div class="">
									<div class="box-gray aligncenter">
										
										<img src="../img/site/html.png" class="ancho_100" title="Desarrollo Web" alt="Desarrollo web utilizando las principales herramientas de innovación">
										<br>
										<br>
										<h4>Desarrollo Web</h4>
											Utilizamos herramientas y estándares de última generación que harán que su producto sea innovador. 
									</div>
									
								</div>
							</div>
						</article>
						<article> 
							<div class="col-xs-12 col-md-4">
								<div class="">
									<div class="box-gray aligncenter">
										
										<img src="../img/site/SEO.png" class="ancho_90" title="SEO" alt="optimización de motores de búsqueda">
										<br>
										<h4>Posicionamiento SEO</h4>
											Si Google o ningún buscador ubican lo que ofreces en la web, no existes. Optimizamos tu sitio para que seas localizado fácilmente.										
									</div>
									
								</div>
							</div>
						</article>


				</div>
				<div class="row">
					<div class="col-xs-12 col-md-12 text-center">

							<a href="../index.php#planes">	<img class="hidden-xs" src="../img/site/alojamiento.png"  title="Hosting Alojamiento Dominio Domain" alt="Alojamiento Gratis Hosting Gratis Nombre de dominio free Domain"> <img class="visible-xs ancho_100" src="../img/site/alojamiento.png" title="Hosting Alojamiento Dominio Domain" alt="Alojamiento Gratis Hosting Gratis Nombre de dominio free Domain"> </a>

					</div>

				</div> 
			
					
		</div>
	</section>
	<!-- end contenido -->
	<!-- start Footer -->
		<?php $base_url='../'; include ('../comunes/footer.php'); ?>
	<!-- end Footer -->
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active" title="ir al arriba"></i></a>
<!-- javascript ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery.js"></script>
<script src="../js/jquery.easing.1.3.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.fancybox.pack.js"></script>
<script src="../js/jquery.fancybox-media.js"></script>
<script src="../js/google-code-prettify/prettify.js"></script>
<script src="../js/portfolio/jquery.quicksand.js"></script>
<script src="../js/portfolio/setting.js"></script>
<script src="../js/jquery.flexslider.js"></script>
<script src="../js/animate.js"></script>
<script src="../js/custom.js"></script>
</body>
</html>