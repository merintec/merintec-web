<!DOCTYPE html>
<?php include('../comunes/variables.php'); ?>
<html lang="es">
<head>
<meta charset="utf-8">
<title><?php echo $var_titulo; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="<?php echo $var_descripcion; ?>" />
<meta name="author" content="<?php echo $var_author; ?>" />
<!-- css -->
<link href="../css/bootstrap.min.css" rel="stylesheet" />
<link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="../css/jcarousel.css" rel="stylesheet" />
<link href="../css/flexslider.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
<!-- Theme skin -->
<link href="../skins/default.css" rel="stylesheet" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<div id="wrapper">
	<!-- start header -->
		<?php $base_url='../'; include ('../comunes/cabecera.php'); ?>
	<!-- end header -->
	<section id="featured">
	
		<div class="container">

				<div class="row">
					<div class="col-md-12 col-xs-12 text-center">	<h1 style="margin-bottom: 0px;"><span class="subtitulo1">Desarrollo</span> de Sistemas</h1> </div> 
				</div>	
				<div class="row">

						<article> 
							<div class="col-xs-12 col-md-6">
								<div class="">
									<div class="box-gray aligncenter">
										
										<img src="../img/site/lenguajes.png" class="ancho_100" title="Desarrollo y Programación de sistemas" alt="Programación de sistemas, desarrollo de software ">
										<h4>Desarrollo de Sistemas</h4>
											Creamos Sistemas para Soluciones Empresariales a la medida de las necesidades del cliente, llevando a cabo el proyecto con todos los estándares de Ingeniería de Software, adaptándonos a las diferentes plataformas tecnológicas.  
									</div>
									
								</div>
							</div>
						</article>
						<article> 
							<div class="col-xs-12 col-md-6">
								<div class="">
									<div class="box-gray aligncenter">
										
										<img src="../img/site/ios_android.png" class="ancho_75" title="Desarrollo de Apps" alt="Creación de aplicaciones moviles, desarrollo de app">
										<br>
										<br>
										<h4>Aplicaciones Móviles</h4>
											Necesitas una App, dejalo en manos de profesionales, trabajamos con las plataformas mas populares, Android y Apple(IOS). Determinamos su necesidad, la diseñamos y la hacemos realidad.   
									</div>
									
								</div>
							</div>
						</article>
						


				</div>
				
			
					
		</div>
	</section>
	<!-- end contenido -->
	<!-- start Footer -->
		<?php $base_url='../'; include ('../comunes/footer.php'); ?>
	<!-- end Footer -->
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active" title="ir al arriba"></i></a>
<!-- javascript ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery.js"></script>
<script src="../js/jquery.easing.1.3.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.fancybox.pack.js"></script>
<script src="../js/jquery.fancybox-media.js"></script>
<script src="../js/google-code-prettify/prettify.js"></script>
<script src="../js/portfolio/jquery.quicksand.js"></script>
<script src="../js/portfolio/setting.js"></script>
<script src="../js/jquery.flexslider.js"></script>
<script src="../js/animate.js"></script>
<script src="../js/custom.js"></script>
</body>
</html>