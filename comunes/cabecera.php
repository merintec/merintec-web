<!-- Cabecera de la pagina -->
<!-- requiere variable $base_url para determinar si esta en raiz $base_url = '' para determinar que esta en un directorio interno $base_url='../' -->
<!-- requiere variable $activo para determinar cual menu está activo p.ejm: $activo = "Servicios" si está vacio se asume que es inicio -->
	<header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo $base_url;?>index.php" title="Merintec"><div class="logo">&nbsp;</div></a>
                </div>
                <div class="pull-right social_top">
					<a href="https://www.facebook.com/merintec"><i class="fa fa-facebook-square fa-3x s-icon" title="Facebook"></i></a>
					<a href="https://twitter.com/merintec"><i class="fa fa-twitter-square fa-3x s-icon" title="Twitter"></i></a>
				</div>
                <div class="navbar-collapse collapse margin-top4">
                    <ul class="nav navbar-nav">
                        <li class="<?php if ($activo=='' || $activo=='Incio') { echo 'active'; } ?>"><a href="<?php echo $base_url;?>index.php">Inicio</a></li>

                        <li class="<?php if ($activo=='Servicios') { echo 'active'; } ?>"><a href="<?php echo $base_url;?>index.php#servicios">Servicios</a></li>
                        <li class="<?php if ($activo=='Planes') { echo 'active'; } ?>"><a href="<?php echo $base_url;?>index.php#planes">Planes Web</a></li>
                        <li class="<?php if ($activo=='Trabajos') { echo 'active'; } ?>"><a href="<?php echo $base_url;?>index.php#planes">Trabajos Recientes</a></li>
                        <li class="<?php if ($activo=='Contacto') { echo 'active'; } ?>"><a href="<?php echo $base_url;?>frontend/contacto.php">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>