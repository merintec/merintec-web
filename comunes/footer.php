<!-- Pie de la pagina -->
<!-- requiere variable $base_url para determinar si esta en raiz $base_url = '' para determinar que esta en un directorio interno $base_url='../' -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-12">
					<div class="widget">
						<h4 class="widgetheading">Visitanos y gustosamente te atenderemos en</h4>
						<address>
						<strong>AC Merintec RL</strong><br>
						 CC Matriz Plaza, Piso 2<br>
						 Local 28. Ejido, Mérida<br>Venezuela</address>
						<i class="fa fa-phone-square fa-3x pull-left hidden-xs"></i>(0274) 2211508 <br>(0274) 3113443 <br>
						<br><i class="fa fa-envelope fa-2x pull-left hidden-xs"></i><span class="hidden-xs"><a href="<?php echo $base_url;?>frontend/contacto.php">contacto@merintec.com.ve</a></span><span class="visible-xs"><a href="mailto:contacto@metintec.com.ve">contacto@merintec.com.ve</a></span>
					</div>
				</div>
				
				<div class="col-md-4 col-xs-12">
				<br>
				<h4> Menú Principal </h4>
				<a class="vinculo_footer" href="<?php echo $base_url;?>index.php">	Inicio </a> <br>
				<a class="vinculo_footer" href="<?php echo $base_url;?>frontend/soporte-tecnico-redes.php">Soporte Técnico - Redes </a> <br>
				<a class="vinculo_footer" href="<?php echo $base_url;?>frontend/diseno-desarrollo-web.php">Diseño - Desarrollo Web </a> <br>
				<a class="vinculo_footer" href="<?php echo $base_url;?>frontend/desarrollo-programacion-sistemas.php">Desarrollo y Programación de Sistemas</a> <br>
				<a class="vinculo_footer" href="<?php echo $base_url;?>index.php#planes">Planes Web</a> <br>
				<a class="vinculo_footer" href="<?php echo $base_url;?>index.php#trabajos">Trabajos Recientes</a> <br>
				<a class="vinculo_footer" href="<?php echo $base_url;?>frontend/contacto.php">Contacto</a> <br>





				
				</div>
				<div class="col-md-2 hidden-xs text-center">
					<h4 class="widgetheading">Redes Sociales</h4>
					<a href="https://www.facebook.com/merintec"><i class="fa fa-facebook-square fa-3x" title="Facebook"></i></a><br><br>
					<a href="https://twitter.com/merintec"><i class="fa fa-twitter-square fa-3x" title="Twitter"></i></a>
				</div>
			</div>
		</div>
		<div id="sub-footer">
			<div class="container">
				<div class="row">
					<div class="col-xs-6">
						<div class="copyright">
							<p>
								<span>&copy; Merintec <?php echo date('Y'); ?> todos los derechos reservados. </span><a href="http://merintec.com.ve" target="_blank">www.merintec.com.ve</a>
							</p>
						</div>
					</div>
					<div class="col-xs-6">
						<ul class="social-network visible-xs">
							<li><a href="https://twitter.com/merintec" data-placement="top" title="Twitter"><i class="fa fa-twitter-square fa-3x pull-right"></i></a></li>
							<li><a href="https://www.facebook.com/merintec" data-placement="top" title="Facebook"><i class="fa fa-facebook-square fa-3x pull-right"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>
