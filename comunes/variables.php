<?php 
//////////////// Variables generales del Sitio ///////////////////////
$var_titulo = ".:: Merintec ::. Diseño y Desarrollo de Paginas Web, Diseño Adapatable a todo tipo de pantalla, WEB Responsive Desing, Redes, Soporte Técnico, Desarrollo de Sistemas en Mérida Venezuela";
$var_descripcion = "Empresa lider en Diseño y Desarrollo de Paginas Web, Diseño Adapatable a todo tipo de Dispositivo, WEB Responsive Desing, Redes, Soporte Técnico, Desarrollo de Sistemas y mucho mas en Mérida, Venezuela";
$var_descripcion_contacto = "Contactanos y conoce mas sobre nuestros productos y servicios. Todas las soluciones tecnológicas en una sola empresa";
$var_descripcion_programacion = "Creamos Sistemas para Soluciones Empresariales a la medida de las necesidades de tu empresa. Multiples plataformas para escritorio y smartphones";
$var_descripcion_web = "Mejora tu presencia en la web. Desarrollamos sitios web de alto desempeño, fáciles de usar. Incrementa tus clientes, llena tu cartera de clientes";
$var_descripcion_soporte = "Mantenimineto o Reparación de equipos de equipos de computación, windows, linux. Instalación u optimizacion de Redes locales.";
$var_author = 'Merintec';
$var_keywords = 'Merida, Venezuela, Diseño web, Diseño de paginas web, diseño de websites, Diseño, web, paginas web, SEO, Optimización para motores de busquedas, equipos de computación, diseño adaptativo, diseño adaptado, profesionales, desarrollo, sistemas informaticos, mantenimineto de computadoras, reparación de computador, redes, redes de area local, redes inalambricas';
$analyticsgoogle="<script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-86436361-1', 'merintec.com.ve'); ga('send', 'pageview'); </script>"; ?>